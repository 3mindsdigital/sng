<!DOCTYPE html>
<html lang="en">
<?php include_once('./common/header.php'); ?>

<?php include_once('./common/navbar.php'); ?>


<!--about us -->
<div class="margin-top-20">
    <div class="container">
        <div class="section-title style10 text-center">
            <h3>About us</h3>
            <div class="sub-title">Get to know us</div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-sm-12 col-md-8">
                <div class="video  style2 video-lightbox">
                    <img src="images/bg-video.jpg" alt="">
                </div>
                <div class="montserratLight margin-top-30 text-center">
                    <p><b>Scarves and Glitter</b> is India’s leading brand that specializes in scarves, tunics and accessories that suit the Indian buyers taste. We design and manufacture our products locally and ensure they’re in line with the latest trends dominating the global runways. Our products are contemporary and offer a perfect amalgamation of modern and traditional designs that suit the taste of women from all walks of life; from the modern fashionista to the multi-tasking homemaker, there is a design and style to satiate everyone’s taste. </p>
                    <p>Scarves and Glitters is a sister company of the brand Kashkha. We started our journey in 2008 foraying into an extremely trendy and colorful collection of scarves that have proven popular with shoppers since then have grown to expand our portfolio to include Tunics, Accessories such as jewelry & bags. With exclusive in-house designers, internationally sourced materials and an attitude tuned in to deliver the best in quality, Scarves and Glitters is a powerhouse when it comes to scarves and accessories.</p>
                    <img src="images/signature.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./about us -->
<!-- footer-->
<?php include_once('./common/footer.php'); ?>

</html>