<!DOCTYPE html>
<html lang="en">
<?php include_once('./common/header.php'); ?>

<?php include_once('./common/navbar.php'); ?>


<div class="main-container shop-with-banner left-slidebar">
    <div class="container">
        <div class="section-title style6 text-center margin-bottom-40">
            <h3>Store Locator</h3>
            <span class="sub-title">Come find us</span>
        </div>
        <div class="breadcrumbs style2">
            <a href="#">Home</a>
            <span>Store_Locator</span>
        </div>
    <div class="margin-top-25">
    <div class="container">
        <div class="row group-banner-text">
            <div class="col-sm-4 item">
                <div class="block-banner-text style2">
                    <div class="image">
                        <a href="#"><img src="images/b/481.jpg" alt=""></a>
                    </div>
                    <div class="banner-content">
                        <div class="inner">
                            <h3 class="title">S&G Phoenix, Pune</h3>
                            <a href="#" class="banner-link">Scarves & Glitters, Unit F-09, 1st Floor, Phoenix Market city, Viman Nagar, Nagar Road, Pune – 411014, Maharashtra.</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 item">
                <div class="block-banner-text style2">
                    <div class="image">
                        <a href="#"><img src="images/b/485.jpg" alt=""></a>
                    </div>
                    <div class="banner-content">
                        <div class="inner">
                            <h3 class="title">S&G Mumbai Airport</h3>
                            <a href="#" class="banner-link">Scarves & Glitters, Terminal 1B/SHA-2, Chhatrapati Shivaji International Airport, Ville Parle (E), Mumbai – 400099, Maharashtra.</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 item">
                <div class="block-banner-text style2">
                    <div class="image">
                        <a href="#"><img src="images/b/483.jpg" alt=""></a>
                    </div>
                    <div class="banner-content">
                        <div class="inner">
                            <h3 class="title">Kashkha Seasons Mall, Pune</h3>
                            <a href="#" class="banner-link">Kashkha, Shop No. G-8, Seasons Mall, Magarpatta City, Hadapsar, Pune – 411013, Maharashtra.</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row group-banner-text">
            <div class="col-sm-4 item">
                <div class="block-banner-text style2">
                    <div class="image">
                        <a href="#"><img src="images/b/484.jpg" alt=""></a>
                    </div>
                    <div class="banner-content">
                        <div class="inner">
                            <h3 class="title">Kashkha Chennai</h3>
                            <a href="#" class="banner-link">Kashkha, Shop No. 155, Express Avenue Mall, Mount Road, Annasalai, Chennai – 600001, Tamil Nadu.</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 item">
                <div class="block-banner-text style2">
                    <div class="image">
                        <a href="#"><img src="images/b/482.jpg" alt=""></a>
                    </div>
                    <div class="banner-content">
                        <div class="inner">
                            <h3 class="title">S&G Hyderabad</h3>
                            <a href="#" class="banner-link">Scarves & Glitters, Unit No. KI-11, Domestic Terminal, Rajiv Gandhi International Airport, Shamshabad, Hyderabad – 500409, Andhra Pradesh.</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 item">
                <div class="block-banner-text style2">
                    <div class="image">
                        <a href="#"><img src="images/b/486.jpg" alt=""></a>
                    </div>
                    <div class="banner-content">
                        <div class="inner">
                            <h3 class="title">S&G Hyderabad 2</h3>
                            <a href="#" class="banner-link">Scarves & Glitters, Unit No. IK-02, International Terminal, Rajiv Gandhi International Airport, Shamshabad, Hyderabad – 500409, Andhra Pradesh.</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
        <div class="section-brand-slide">
            <div class="section-title style6 text-center margin-bottom-40">
            <h3>Online Stores</h3>
            <span class="sub-title">We are here too!</span>
        </div>
        <div class="brands-slide owl-carousel nav-center-center nav-style7" data-nav="true" data-dots="false" data-loop="true" data-margin="60" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'>
            <a href="#"><img src="images/brands/brand1.png" alt=""></a>
            <a href="#"><img src="images/brands/brand2.png" alt=""></a>
            <a href="#"><img src="images/brands/brand3.png" alt=""></a>
            <a href="#"><img src="images/brands/brand4.png" alt=""></a>
            <a href="#"><img src="images/brands/brand5.png" alt=""></a>
        </div>
    </div>
    </div>
    </div>
</div>
<?php include_once('./common/footer.php'); ?>

</html>