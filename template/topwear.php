<!DOCTYPE html>
<html lang="en">
<?php include_once('./common/header.php'); ?>

<?php include_once('./common/navbar.php'); ?>


<!--about us -->
<div class="banner about-us-banner">
    <div class="banner-content">
        <img src="images/b/1.jpg" alt="">
    </div>
</div>
<div class="container">
    <div class="section-title style10 text-center margin-top-20">
            <h3>Top Wear</h3>
            <div class="sub-title">Get to know us</div>
    </div>
</div>
<div class="main-container no-slidebar">
    <div class="container">
        <div class="row">
            <div class="main-content col-sm-12">
                
                <ul class="product-list-grid desktop-columns-3 tablet-columns-3 mobile-columns-1 row flex-flow">
                    <li class="product-item col-sm-4">
                        <div class="product-inner">
                            <div class="product-thumb has-back-image">
                                <a href="#"><img src="images/products/16.jpg" alt=""></a>
                                <div class="gorup-button">
                                    <a href="#" class="wishlist"><i class="fa fa-amazon"></i></a>
                                    <a href="#" class="compare"><i class="fa fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </li>
                   <li class="product-item col-sm-4">
                        <div class="product-inner">
                            <div class="product-thumb has-back-image">
                                <a href="#"><img src="images/products/17.jpg" alt=""></a>
                                <div class="gorup-button">
                                    <a href="#" class="wishlist"><i class="fa fa-amazon"></i></a>
                                    <a href="#" class="compare"><i class="fa fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="product-item col-sm-4">
                        <div class="product-inner">
                            <div class="product-thumb has-back-image">
                                <a href="#"><img src="images/products/18.jpg" alt=""></a>
                                 <div class="gorup-button">
                                    <a href="#" class="wishlist"><i class="fa fa-amazon"></i></a>
                                    <a href="#" class="compare"><i class="fa fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="product-item col-sm-4">
                        <div class="product-inner">
                            <div class="product-thumb has-back-image">
                                <a href="#"><img src="images/products/19.jpg" alt=""></a>
                                 <div class="gorup-button">
                                    <a href="#" class="wishlist"><i class="fa fa-amazon"></i></a>
                                    <a href="#" class="compare"><i class="fa fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="product-item col-sm-4">
                        <div class="product-inner">
                            <div class="product-thumb has-back-image">
                                <a href="#"><img src="images/products/20.jpg" alt=""></a>
                                 <div class="gorup-button">
                                    <a href="#" class="wishlist"><i class="fa fa-amazon"></i></a>
                                    <a href="#" class="compare"><i class="fa fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="product-item col-sm-4">
                        <div class="product-inner">
                            <div class="product-thumb has-back-image">
                                <a href="#"><img src="images/products/21.jpg" alt=""></a>
                                 <div class="gorup-button">
                                    <a href="#" class="wishlist"><i class="fa fa-amazon"></i></a>
                                    <a href="#" class="compare"><i class="fa fa-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                
            </div>
        </div>
    </div>
</div>
<!-- ./about us -->
<!-- footer-->
<?php include_once('./common/footer.php'); ?>

</html>