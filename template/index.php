<!DOCTYPE html>
<html lang="en">
<?php include_once('./common/header.php'); ?>
<?php include_once('./common/navbar.php'); ?>

<!-- Banner images-->

        <!-- Home slide -->
        <div class="container">
        <div class="home-top">
            <div class="block-category-carousel scrollbar style2 open">
                <a href="#" class="block-toggle"><span class="icon pe-7s-close"></span></a>
                <div class="inner">
                    <h3 class="title">CATEGORIES</h3>
                    <span class="sub-title">Find all items you want by select our featured categories</span>
                    <div class="block-inner owl-carousel" data-nav="false" data-dots="false" data-items="1" data-loop="false" data-autoplay="true">
                        <ul class="list-cat">
                            <li><a href="#"><img src="images/icons/14.png" alt="">Scarves</a></li>
                            <li><a href="#"><img src="images/icons/15.png" alt="">Stoles</a></li>
                            <li><a href="#"><img src="images/icons/16.png" alt="">Top Wear</a></li>
                            <li><a href="#"><img src="images/icons/17.png" alt="">Bottom Wear</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="homeslide20 owl-carousel nav-style4 nav-center-center" data-items="1" data-nav="false" data-dots="true" data-loop="true" data-autoplay="true">
                <img src="images/slides/40.jpg" alt="">
                <img src="images/slides/41.jpg" alt="">
                <img src="images/slides/42.jpg" alt="">
            </div>
        </div>
        </div>
        <!-- ./Home slide -->

<!-- Banner images end-->

<!-- group banner -->
<div class="margin-top-60">
    <div class="container">
         <div class="section-title style6 text-center margin-bottom-40">
            <h3>Categories</h3>
            <span class="sub-title">Apt for your style</span>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <a class="banner-opacity margin-bottom-30" href="#"><img src="images/b/35.jpg" alt=""></a>
                <a class="banner-opacity margin-bottom-30" href="#"><img src="images/b/36.jpg" alt=""></a>
            </div>
            <div class="col-sm-4">
                <a class="banner-opacity margin-bottom-30" href="#"><img src="images/b/37.jpg" alt=""></a>
            </div>
            <div class="col-sm-4">
                <a class="banner-opacity margin-bottom-30" href="#"><img src="images/b/38.jpg" alt=""></a>
                <a class="banner-opacity margin-bottom-30" href="#"><img src="images/b/39.jpg" alt=""></a>
            </div>
        </div>
    </div>
</div>
<!-- ./Group banner -->
<!-- Block group banner-->
<div class="margin-top-30">
    <div class="container">
        <div class="section-title style6 text-center margin-bottom-40">
            <h3>Trending</h3>
            <span class="sub-title">Be the trendsetter</span>
        </div>
        <div class="row">
            <div class="col-sm-8 col-md-9">
                <div class="row">
                    <div class="col-sm-7 col-md-8">
                        <div class="block-banner-text">
                            <div class="image">
                                <a href="#"><img src="images/categorys/15-2.jpg" alt=""></a>
                            </div>
                            <div class="content">
                                <h2 class="title">On Sale</h2>
                                <h3 class="subtitle">leather watch</h3>
                                <span class="text"><a href="#">2016 Collection</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 col-md-4">
                        <div class="block-text-border">
                            <div class="content">
                                <h2 class="title">Lookbook</h2>
                                <h3 class="subtitle">New collection</h3>
                                <a href="#" class="link">Explore Now!</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5 col-md-4 padding-right-5">
                        <a href="#" class="banner-border"><img alt="" src="images/b/34.jpg"></a>
                    </div>
                    <div class="col-sm-7 col-md-8">
                        <div class="block-banner-text">
                            <div class="image">
                                <a href="#"><img src="images/categorys/15-4.jpg" alt=""></a>
                            </div>
                            <div class="content">
                                <h2 class="title">New Arrival</h2>
                                <h3 class="subtitle">diamond jewelry</h3>
                                <span class="text"><a href="#">sale 30% off</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-3">
                <a href="#" class="banner-border"><img alt="" src="images/b/33.jpg"></a>
            </div>
        </div>
    </div>
</div>

<!-- Blog -->
<div class="margin-top-50 margin-bottom-40">
    <div class="container">
        <div class="section-title style6 text-center margin-bottom-40">
            <h3>we are here!</h3>
            <span class="sub-title">Come find us</span>
        </div>
        <div class="lastest-blog style5 nav-center-center nav-style6 owl-carousel no-overflow"  data-nav="true" data-dots="false" data-loop="true" data-margin="30" data-responsive='{"0":{"items":1},"600":{"items":1},"1000":{"items":3}}'>
            <div class="item-blog">
                <div class="thumb"><a href="#"><img src="images/blogs/7.jpg" alt=""></a></div>
                <div class="info">
                    <h3 class="blog-title"><a href="#">S&G Phoenix, Pune</a></h3>
                    <a class="readmore" href="#">Scarves & Glitters, Terminal 1B/SHA-2, Chhatrapati Shivaji International Airport, Ville Parle (E), Mumbai – 400099, Maharashtra. </a>
                </div>
            </div>
            <div class="item-blog">
                <div class="thumb"><a href="#"><img src="images/blogs/5.jpg" alt=""></a></div>
                <div class="info">
                    <h3 class="blog-title"><a href="#">S&G Mumbai Airport</a></h3>
                    <a class="readmore" href="#">Scarves & Glitters, Terminal 1B/SHA-2, Chhatrapati Shivaji International Airport, Ville Parle (E), Mumbai – 400099, Maharashtra.</a>
                </div>
            </div>
            <div class="item-blog">
                <div class="thumb"><a href="#"><img src="images/blogs/6.jpg" alt=""></a></div>
                <div class="info">
                    <h3 class="blog-title"><a href="#">Kashkha Seasons Mall, Pune</a></h3>
                    <a class="readmore" href="#">Kashkha, Shop No. G-8, Seasons Mall, Magarpatta City, Hadapsar, Pune – 411013, Maharashtra. </a>
                </div>
            </div>
            <div class="item-blog">
                <div class="thumb"><a href="#"><img src="images/blogs/9.jpg" alt=""></a></div>
                <div class="info">
                    <h3 class="blog-title"><a href="#">Kashkha Chennai</a></h3>
                    <a class="readmore" href="#">Kashkha, Shop No. 155, Express Avenue Mall, Mount Road, Annasalai, Chennai – 600001, Tamil Nadu. </a>
                </div>
            </div>
            <div class="item-blog">
                <div class="thumb"><a href="#"><img src="images/blogs/8.jpg" alt=""></a></div>
                <div class="info">
                    <h3 class="blog-title"><a href="#">S&G Hyderabad</a></h3>
                    <a class="readmore" href="#">Scarves & Glitters, Unit No. KI-11, Domestic Terminal, Rajiv Gandhi International Airport, Shamshabad, Hyderabad – 500409, Andhra Pradesh.</a>
                </div>
            </div>
            <div class="item-blog">
                <div class="thumb"><a href="#"><img src="images/blogs/h2.jpg" alt=""></a></div>
                <div class="info">
                    <h3 class="blog-title"><a href="#">S&G Hyderabad 2</a></h3>
                    <a class="readmore" href="#">Scarves & Glitters, Unit No. IK-02, International Terminal, Rajiv Gandhi International Airport, Shamshabad, Hyderabad – 500409, Andhra Pradesh. </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./Blog -->

<!-- ./Block group banner-->
    <div class="container">
        <div class="section-title style6 text-center margin-bottom-20 margin-top-40">
                <h3>Contact Us!</h3>
        </div>
    <div class="margin-top-20">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <div class="video video-lightbox">
                        <img src="images/bg_video.png" id="contact_image" alt="">
                    </div>
                </div>
                <div class="col-sm-12 col-md-5">
                    <div class="newsletter">
                        <div class="section-title text-center"><h3>NEWSLETTER</h3></div>
                        <i class="newsletter-info">Sign up for Our Newsletter &amp; Promotions</i>
                        <form class="form-newsletter">
                          <input type="text" name="newsletter" placeholder="Your email address here..." value="">
                          <span><button class="newsletter-submit" type="submit">SIGNUP</button></span>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="section-title style6 text-center margin-bottom-40 margin-top-50 ">
            <h3>Instagram!</h3>
            <span class="sub-title">Follow us!</span>
  </div>      
<script src="https://apps.elfsight.com/p/platform.js" defer></script>
<div class="elfsight-app-06e66a49-6e38-470e-a01e-b7b6a7cc4f0e"></div>
    

<?php include_once('./common/footer.php'); ?>

</html>