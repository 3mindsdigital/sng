<body class="home">
<div id="box-mobile-menu" class="box-mobile-menu full-height full-width">
    <div class="box-inner">
        <span class="close-menu"><span class="icon pe-7s-close"></span></span>
    </div>
</div>
<div id="header-ontop" class="is-sticky"></div>
<!-- Header -->
<header id="header" class="header style3 style12">
    <div class="container">
       
        <div class="main-header">
            <div class="main-menu-wapper">
                <div class="row">
                     <div class="col-sm-9 col-md-12 col-lg-6 logo-main">
                        <div class="logo">
                            <a href="#"><img src="images/logos/1.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-3 col-md-7 col-lg-6">
                        <ul class="boutique-nav main-menu clone-main-menu">                                      
                            <li class="active menu-item-has-children item-megamenu">
                                <a href="index.php">Home</a>
                                
                            </li>
                            <li class="menu-item-has-children">
                                <a href="about.php">About Us</a>
                            </li>
                            <li class="menu-item-has-children item-megamenu">
                                <a href="#">Categories</a>
                                <span class="arow"></span>
                                    <ul class="sub-menu">
                                        <li><a href="scarves.php">Scarves</a></li>
                                        <li><a href="topwear.php">Top Wear</a></li>
                                        <li><a href="cart.html">Stoles</a></li>
                                        <li><a href="checkout.html">Bottom Wear</a></li>
                                    </ul>
                                
                            </li>
                            <li class="menu-item-has-children item-megamenu">
                                <a href="store-locator.php">Store Locator</a>
                                
                            </li>
                            <li class="menu-item-has-children">
                                <a href="contact.php">Contact Us</a>    
                            </li>
                        </ul>
                        <span class="mobile-navigation"><i class="fa fa-bars"></i></span>
                    </div>          
                   
                </div>
            </div>
        </div>
    </div>
</header>
<!-- ./Header -->