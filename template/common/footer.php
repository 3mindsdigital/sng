<!-- footer-->
<footer class="footer style2 style4">
            <div class="footer-top">
                <div class="container">
                    <div class="widget contact-info">
                        <div class="logo">
                            <a href="#"><img alt="" src="images/logos/1.jpg"></a>
                        </div>
                        <div class="content">
                            <p>5701 Outlets at Tejon Pkwy, Tejon ranch CA 93203 UK.</p>
                            <p class="phone"><i class="fa fa-phone"></i> <span> (+800) 6868 2268</span></p>
                        </div>
                    </div>

                    <ul class="footer-menu">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="about.php">About Us</a></li>
                        <li><a href="#">Categories</a></li>
                        <li><a href="store-locator.php">Store Locator</a></li>
                        <li><a href="#">Terms &amp; Condition</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
            </div>
</footer>
<!-- footer ends -->
    <a href="#" class="scroll_top" title="Scroll to Top" style="display: block;"><i class="fa fa-arrow-up"></i></a>
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="js/Modernizr.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/lightbox.min.js"></script>
    <script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/masonry.js"></script>
    <script type="text/javascript" src="js/functions.js"></script>
</body>