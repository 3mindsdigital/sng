<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <title>Boutique - eCommerce</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/chosen.css">
    <link rel="stylesheet" type="text/css" href="css/lightbox.min.css">
    <link rel="stylesheet" type="text/css" href="css/pe-icon-7-stroke.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Montserrat">
    <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400italic,400,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,100,100italic,300italic,400,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
</head>
<body class="home">
<div id="box-mobile-menu" class="box-mobile-menu full-height full-width">
    <div class="box-inner">
        <span class="close-menu"><span class="icon pe-7s-close"></span></span>
    </div>
</div>
<div id="header-ontop" class="is-sticky"></div>
<!-- Header -->
<header id="header" class="header style2 style9">
    <div class="main-header">
        <div class="container">
            <div class="main-menu-wapper">
                <div class="row">
                    <div class="col-sm-12 col-md-2">
                        <div class="logo">
                            <a href="#"><img src="images/logos/3.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-10">
                        
                        <ul class="boutique-nav main-menu clone-main-menu">                                      
                            <li class="active menu-item-has-children item-megamenu">
                                <a href="index.html">Home</a>
                                
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Pages</a>
                                
                            </li>
                            <li class="menu-item-has-children item-megamenu">
                                <a href="#">Shop</a>
                               
                            </li>
                            <li class="menu-item-has-children item-megamenu">
                                <a href="#">FEATURES</a>
                                
                            </li>
                            <li class="menu-item-has-children">
                                <a href="blogs.html">BLOG</a>
                                
                            </li>
                        </ul>
                        <span class="mobile-navigation"><i class="fa fa-bars"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- ./Header -->
<!-- Banner images-->

        <!-- Home slide -->
<div class="container margin-top-30">
    <div class="home-slide1 style16 owl-carousel nav-style1"  data-nav="true" data-dots="false" data-loop="true" data-autoplay="true" data-margin="10" data-responsive='{"0":{"items":1,"nav":false},"600":{"items":1,"nav":false},"1000":{"items":2}}'>
        <img src="images/slides/16-1.jpg" alt="">
        <img src="images/slides/16-2.jpg" alt="">
    </div>
</div>
        <!-- ./Home slide -->
<!-- block text box -->
<div class="margin-top-30">
    <div class="container">
        <div class="row section-block-text">
            <div class="col-sm-6 item">
                <div class="block-text">
                    <p>boutique is not over with fashion fever week! </p>
                    <p class="primary">ends Dec 29th</p>
                </div>
            </div>
            <div class="col-sm-6 item">
                <div class="block-text" style="background-color: #9ec8b3;">
                    <p>boutique exclusives up to 90% + extra 10% </p>
                    <p class="primary">ends Dec 29th</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./block text box -->
<!-- Banner images end-->

<div class="container">
            <div class="section-title style4 text-center">
                <span class="text-head">C</span>
                <h3>Categories</h3>
            </div>
        <div class="margin-top-30">
            <div class="row">
                <div class="col-sm-5">
                    <a class="banner-border" href="#"><img src="images/b/22.jpg" alt=""></a>
                </div>
                <div class="col-sm-4">
                    <div class="banner-text style2">
                        <div class="image">
                            <a class="banner-opacity" href="#"><img src="images/b/23.jpg" alt=""></a>
                        </div>
                        <div class="content-text">
                            <h3 class="title">WOMENSWEAR</h3>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="banner-text style2">
                        <div class="image">
                            <a class="banner-opacity" href="#"><img src="images/b/24.jpg" alt=""></a>
                        </div>
                        <div class="content-text">
                            <h3 class="title">BAGS</h3>
                        </div>
                    </div>
                    <div class="banner-text style2">
                        <div class="image ">
                            <a class="banner-opacity" href="#"><img src="images/b/25.jpg" alt=""></a>
                        </div>
                        <div class="content-text">
                            <h3 class="title">SHOES</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
    <!-- Group banner-->
        <div class="margin-top-25">
            <div class="section-title style6 text-center margin-bottom-40">
            <h3>Trending</h3>
            <span class="sub-title">Be the trendsetter</span>
        </div>
            <div class="group-banner-masonry">
                <div class="grid-sizer"></div>
                <div class="banner-masonry-item">
                    <div class="inner">
                        <img src="images/b/15-20.png" alt="">
                        <div class="content">
                            <h2 class="title">trending</h2>
                            <h3 class="subtitle">autumn fashion</h3>
                            <a class="link" href="#">shop now!</a>
                        </div>
                    </div>
                </div>
                <div class="banner-masonry-item">
                    <div class="inner">
                        <img src="images/b/15-21.png" alt="">
                        <div class="content">
                            <h2 class="title">trending</h2>
                            <h3 class="subtitle">autumn fashion</h3>
                            <a class="link" href="#">shop now!</a>
                        </div>
                    </div>
                </div>

                <div class="banner-masonry-item">
                    <div class="inner">
                        <img src="images/b/15-22.png" alt="">
                        <div class="content">
                            <h2 class="title">trending</h2>
                            <h3 class="subtitle">autumn fashion</h3>
                            <a class="link" href="#">shop now!</a>
                        </div>
                    </div>
                </div>
                <div class="banner-masonry-item">
                    <div class="inner">
                        <img src="images/b/15-23.png" alt="">
                        <div class="content">
                            <h2 class="title">trending</h2>
                            <h3 class="subtitle">autumn fashion</h3>
                            <a class="link" href="#">shop now!</a>
                        </div>
                    </div>
                </div>
                <div class="banner-masonry-item banner-masonry-item-2x">
                    <div class="inner">
                        <img src="images/b/15-24.png" alt="">
                        <div class="content">
                            <h2 class="title">trending</h2>
                            <h3 class="subtitle">autumn fashion</h3>
                            <a class="link" href="#">shop now!</a>
                        </div>
                    </div>
                </div>
                <div class="banner-masonry-item banner-masonry-item-2x">
                    <div class="inner">
                        <img src="images/b/15-25.png" alt="">
                        <div class="content">
                            <h2 class="title">trending</h2>
                            <h3 class="subtitle">autumn fashion</h3>
                            <a class="link" href="#">shop now!</a>
                        </div>
                    </div>
                </div>
                <div class="banner-masonry-item">
                    <div class="inner">
                        <img src="images/b/15-26.png" alt="">
                        <div class="content">
                            <h2 class="title">trending</h2>
                            <h3 class="subtitle">autumn fashion</h3>
                            <a class="link" href="#">shop now!</a>
                        </div>
                    </div>
                </div>
                <div class="banner-masonry-item">
                    <div class="inner">
                        <img src="images/b/15-27.png" alt="">
                        <div class="content">
                            <h2 class="title">trending</h2>
                            <h3 class="subtitle">autumn fashion</h3>
                            <a class="link" href="#">shop now!</a>
                        </div>
                    </div>
                </div>
                <div class="banner-masonry-item">
                    <div class="inner">
                        <img src="images/b/15-28.png" alt="">
                        <div class="content">
                            <h2 class="title">trending</h2>
                            <h3 class="subtitle">autumn fashion</h3>
                            <a class="link" href="#">shop now!</a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
<!-- ./Group banner-->    



    <!-- Blog -->
    <div class="margin-top-95 section-lasttest-blog">
        <div class="container">
            <div class="section-title style4 text-center">
                <span class="text-head">F</span>
                <h3>find us here</h3>
            </div>
            <div class="lastest-blog style3 nav-center-center nav-style7 owl-carousel" data-nav="true" data-dots="false" data-loop="true" data-margin="30" data-responsive='{"0":{"items":1},"600":{"items":2},"1000":{"items":3}}'>
                <div class="item-blog">
                    <h3 class="blog-title"><a href="#">The Adventure of the Property</a></h3>
                    <div class="thumb"><a href="#"><img src="images/blogs/14-18.png" alt=""></a></div>
                    <div class="metas">
                        <span><a href="#">Webdesign</a></span>
                        <span class="date">15 Jan 2015</span>
                        <span class="comment">36 comments</span>
                    </div>
                    <div class="desc">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
                <div class="item-blog">
                    <h3 class="blog-title"><a href="#">A Kid Who Doesn’t Grow Up</a></h3>
                    <div class="thumb"><a href="#"><img src="images/blogs/14-19.png" alt=""></a></div>
                    <div class="metas">
                        <span><a href="#">Webdesign</a></span>
                        <span class="date">15 Jan 2015</span>
                        <span class="comment">36 comments</span>
                    </div>
                    <div class="desc">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
                <div class="item-blog">
                    <h3 class="blog-title"><a href="#">Entry from An Shopholic</a></h3>
                    <div class="thumb"><a href="#"><img src="images/blogs/14-20.png" alt=""></a></div>
                    <div class="metas">
                        <span><a href="#">Webdesign</a></span>
                        <span class="date">15 Jan 2015</span>
                        <span class="comment">36 comments</span>
                    </div>
                    <div class="desc">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- ./Blog --><!-- footer-->
    <footer class="footer style2 style4">
            <div class="footer-top">
                <div class="container">
                    <div class="widget contact-info">
                        <div class="logo">
                            <a href="#"><img alt="" src="images/logos/1.png"></a>
                        </div>
                        <div class="content">
                            <p>5701 Outlets at Tejon Pkwy, Tejon ranch CA 93203 UK.</p>
                            <p class="phone"><i class="fa fa-phone"></i> <span> (+800) 6868 2268</span></p>
                        </div>
                    </div>

                    <ul class="footer-menu">
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Order History</a></li>
                        <li><a href="#">Returns</a></li>
                        <li><a href="#">Custom Service</a></li>
                        <li><a href="#">Terms &amp; Condition</a></li>
                        <li><a href="#">Order History</a></li>
                    </ul>
                </div>
            </div>
    </footer>
<!-- footer ends -->
    <a href="#" class="scroll_top" title="Scroll to Top" style="display: block;"><i class="fa fa-arrow-up"></i></a>
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="js/Modernizr.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/lightbox.min.js"></script>
    <script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/masonry.js"></script>
    <script type="text/javascript" src="js/functions.js"></script>
</body>
</html>